"""
Miguel de la Cruz Cabello
Smart Industry Systems 
09/07/2024

“During the preparation of this work, the author used CHAT-GPT 3.5 in order to
retrieve pseudocode of the Python script and to get explanation of the SAREF ontology and the RDFLib documentation as he
did not work with it previously. After using this tool/service, the author reviewed 
and edited the content as needed and take(s) full responsibility for the content of the work.”

"""
import sys
import os
import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, RDFS, XSD


# Check for correct number of arguments
if len(sys.argv) != 2:
    print("Usage: python transformer.py path/to/dataset.csv")
    sys.exit(1)

# Read the dataset
input_csv_path = sys.argv[1]
print(f"Reading dataset from: {input_csv_path}")
data = pd.read_csv(input_csv_path, nrows=1000) # I read only the first 1000 rows as I was getting memory issues when reading the entire dataset 
print(f"Dataset loaded successfully. Number of records: {len(data)}")

# Create the graph
g = Graph()
SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/")
XS = Namespace('https://www.w3.org/2001/XMLSchema')

# Bind namespaces
g.bind("saref", SAREF)
g.bind("ex", EX)
g.bind('xs', XS)

# Create RDF URI's related to the data
building = URIRef("http://example.org/Building")
industrial_building = URIRef("http://example.org/IndustrialBuilding")
public_building = URIRef("http://example.org/PublicBuilding")
residential_building = URIRef("http://example.org/ResidentialBuilding")

# Define building subtypes
g.add((industrial_building, RDFS.subClassOf, building))
g.add((public_building, RDFS.subClassOf, building))
g.add((residential_building, RDFS.subClassOf, building))

# Triplification
print("Starting triplification...")
for col in data.columns:
    if col in ["utc_timestamp", "cet_cest_timestamp", "interpolated"]:
        continue

    building_type = col.split('_')[2]
    building_uri = URIRef(f"http://example.org/{building_type}")

    if "industrial" in col:
        g.add((building_uri, RDF.type, industrial_building))
    elif "public" in col:
        g.add((building_uri, RDF.type, public_building))
    elif "residential" in col:
        g.add((building_uri, RDF.type, residential_building))
    else:
        print(f"Unknown building: {col}")

    # Add meters to buildings
    meter = URIRef(f"http://example.org/{col}")

    g.add((meter, RDF.type, SAREF.Meter))
    g.add((meter, RDFS.label, Literal(col.replace("_", " "), lang="en")))
    g.add((meter, EX.locatedIn, building_uri))

    # Here I add the measurements to the graph
    print(f"Processing {col}...")
    for i, row in data.iterrows():
        time = row["utc_timestamp"]
        value = row[col]
        if pd.isnull(value):
            continue
        
        # Add measurements
        measurement_uri = URIRef(f"http://example.org/{col}_{i}")
        g.add((measurement_uri, RDF.type, SAREF.Measurement))
        g.add((measurement_uri, SAREF.hasTimestamp, Literal(time, datatype=XSD.dateTime)))
        g.add((measurement_uri, SAREF.hasValue, Literal(value, datatype=XS.decimal)))
        g.add((measurement_uri, SAREF.isMeasuredIn, Literal("kW")))

print("RDF graph creation completed.")

# Sderialize the graph and save it in a turtle file
output_turtle_path = os.path.join(os.path.dirname(__file__), 'graph.ttl')
g.serialize(destination=output_turtle_path, format='turtle')

print(f"RDF graph has been serialized to {output_turtle_path}")

